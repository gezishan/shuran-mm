$(document).ready(function() {

	var pos = 0;
	// 图片定时器
	var timer = setInterval(fn, 3000);

	function fn() {
		$(".pic_item").eq(pos).show().siblings().hide();
		$(".circle li").eq(pos).addClass("current").siblings().removeClass("current");
		pos = (pos + 1) % $(".pic_item").size();
	}
	fn();  //直接运行一次，解决刚进网页时要多等一次定时的时间问题

	//鼠标移入清除定时器
	$(".pictures").mouseenter(function() {
		$(".btn_left").show();
		$(".btn_right").show();
		clearInterval(timer);
	});

	//鼠标移出启动定时器
	$(".pictures").mouseleave(function() {
		$(".btn_left").hide();
		$(".btn_right").hide();
		clearInterval(timer);
		timer = setInterval(fn, 3000);
	});

	// 右侧按钮事件
	$(".btn_right").click(fn);

	// 左侧按钮事件
	$(".btn_left").click(function() {
		$(".pic_item").eq(pos).show().siblings().hide();
		$(".circle li").eq(pos).addClass("current").siblings().removeClass("current");
		pos = (pos - 1) % $(".pic_item").size();
	});

	// 小圆点事件
	$(".circle li").mousemove(function() {
		$(this).addClass("current").siblings().removeClass("current");
		var position = $(".circle li").index($(this));
		$(".pic_item").eq(position).show().siblings().hide();
	});

	// 小图按钮事件
	$(".success_btn").click(function() {
		$(".success_item:lt(4)").toggle();
	});

	//我们做什么图片
	$(".about_list").hover(function(){
		$(this).find($(".about_list img")).css({width:"140px",height:"140px"});
	},function(){
		$(this).find($(".about_list img")).css({width:"135px",height:"135px"});
	});

	// QQ在线客服事件

	//定义两个状态都为一个div的时候
	// $(".qq1").click(function(){
	// 	$(".qq1").hide();
	// 	$(".qq2").show();
	// });
	// $(".qq2").click(function(){
	// 	$(".qq2").hide();
	// 	$(".qq1").show();
	// });

	// QQ在线客服事件
	var num = 0;
	$(".qq").click(function() {
		if (num % 2 == 0) {
			$(".qq").animate({
				right: 0
			}, 500);
		} else {
			$(".qq").animate({
				right: -129
			}, 500);
		}
		num++;
	});

});